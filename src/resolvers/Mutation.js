const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {randomBytes} = require('crypto');
const {promisify} = require('util');
const {transport, makeANiceEmail} = require('../mail');
const {hasPermission} = require('../utils');

const Mutation = {
  async createUser(parent, args, ctx, info) {
    const user = await ctx.db.mutation.createUser({
      data: {
        ...args
      }
    }, info);

    return user;
  },

  async createItem(parent, args, ctx, info) {
    if(!ctx.request.userId){
      throw new Error('You must be logged in to do that!');
    }

    const item = await ctx.db.mutation.createItem({
      data: {
        owner: {
          connect: {
            id: ctx.request.userId
          }
        },
        ...args
      }
    }, info);

    return item;
  },

  async signup(parent, args, ctx, info){
    args.email = args.email.toLowerCase();
    // salt password
    const password = await bcrypt.hash(args.password, process.env.APP_SALT || 10);

    // create the user in db
    const user =  await ctx.db.mutation.createUser({
      data: {
        ...args,
        password,
        permissions: {set: ['USER']}
      }
    }, info);

    // create the JWT token
    const token = jwt.sign({userId: user.id}, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year
    });

    return user;
  },

  async signin(parent, {email, password}, ctx, info){
    // lookup user by email
    const user = await ctx.db.query.user({where: {email}});
    if(!user){
      throw new Error(`No such user found for email "${email}"`)
    }
    // check password
    const valid = await bcrypt.compare(password, user.password);
    if(!valid){
      throw new Error('Invalid Password!');
    }
    // generate JWT Token
    const token = jwt.sign({userId: user.id}, process.env.APP_SECRET);
    // set cookie
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year
    });
    // return user
    return user;
  },

  signout(parent, args, ctx, info){
    ctx.response.clearCookie('token');
    return {message: 'Goodbye!'};
  },

  async requestReset(parent, {email}, ctx, info){
    // lookup user by email
    const user = await ctx.db.query.user({where: {email}});
    if(!user){
      throw new Error(`No such user found for email "${email}"`)
    }
    // set reset token and expiry
    const randomBytesPromisifies = promisify(randomBytes);
    const resetToken = (await randomBytesPromisifies(20)).toString('hex');
    const resetTokenExpiry = Date.now() + (1000 * 60 * 60); // 1 hour
    const res = await ctx.db.mutation.updateUser({
      where: {email},
      data: {
        resetToken,
        resetTokenExpiry,
      }
    });
    // email user reset token
    const mailRes = await transport.sendMail({
      from: 'david@wigly.com',
      to: user.email,
      subject: 'Your Password Reset',
      html: makeANiceEmail(`
        Your Password Reset is here! \n\n
        <a href="${process.env.FRONTEND_URL}/reset?resetToken=${resetToken}">Click here to Reset.<a>
      `)
    });

    return {message: 'Thanks!'}
  },


  async resetPassword(parent, {resetToken, password, confirmPassword}, ctx, info){
    // check if passwords match
    if(password !== confirmPassword){
      throw new Error('Passwords must match.');
    }
    // check if legit reset token
    const [user] = await ctx.db.query.users({where: {
      resetToken,
      resetTokenExpiry_gte: Date.now() - (1000 * 60 * 60),
    }});
    if(!user){
      throw new Error('Reset Token: Invalid or Expired');
    }

    // hash new password
    const hashedPassword = await bcrypt.hash(password, process.env.APP_SALT || 10);

    // update user
    const updatedUser = await ctx.db.mutation.updateUser({
      where: {email: user.email},
      data: {
        password: hashedPassword,
        resetToken: null,
        resetTokenExpiry: null,
      }
    });

    // create the JWT token
    const token = jwt.sign({userId: updatedUser.id}, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year
    });

    return updatedUser;
  },

  async updateUser(parent, {email, name, userId}, ctx, info){
    if(!ctx.request.userId){
      throw new Error('You must be logged in to do that!');
    }

    const isMe = ctx.request.userId === userId;
    const hasPermissions = ctx.request.user.permissions.some(permission => ['ADMIN'].includes(permission));

    if(!isMe && !hasPermissions)
      throw new Error("You don't have permission to do that.");

    return ctx.db.mutation.updateUser({
      data: {
        email,
        name,
      },
      where: {
        id: userId,
      }
    }, info);
  },

  async updatePermissions(parent, args, ctx, info){
    if(!ctx.request.userId){
      throw new Error('You must be logged in to do that!');
    }

    const currentUser = await ctx.db.query.user({
      where: {
        id: ctx.request.userId,
      },
    }, info);

    hasPermission(currentUser, ['ADMIN', 'PERMISSION_UPDATE']);

    return ctx.db.mutation.updateUser({
      data: {
        permissions: {
          set: args.permissions,
        }
      },
      where: {
        id: args.userId,
      }
    }, info);
  },
};

module.exports = Mutation;
