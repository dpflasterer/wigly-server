const {forwardTo} = require('prisma-binding');
const {hasPermission} = require('../utils');

const Query = {
  // ex: prisma forwarding
  // users: forwardTo('db'),
  // ex: prisma manual
  async users(parent, args, ctx, info){
    if(!ctx.request.userId)
      throw new Error('You must be logged in!');

    hasPermission(ctx.request.user, ['ADMIN', 'PERMISSIONUPDATE']);

    return await ctx.db.query.users({}, info);
  },

  async user(parent, {userId, email}, ctx, info){
    if(!ctx.request.userId)
      throw new Error('You must be logged in!');

    hasPermission(ctx.request.user, ['ADMIN']);

    const [user] = await ctx.db.query.users({
      where: {
        OR: [
          {id: userId},
          {email},
        ]
      }
    }, info);

    return user
  },

  item: forwardTo('db'),
  items: forwardTo('db'),

  me(parent, args, ctx, info){
    // check if there is a current user ID
    if(!ctx.request.userId) return null;

    return ctx.db.query.user({
      where: {
        id: ctx.request.userId
      }
    }, info);
  }

};

module.exports = Query;