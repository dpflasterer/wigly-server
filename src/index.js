require('dotenv').config();
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const createServer = require('./createServer');
const db = require('./db');

const server = createServer();

server.express.use(cookieParser());

// decode the JWT so we can get the user ID on each request
server.express.use((req, res, next) => {
  const {token} = req.cookies;

  if(token){
    const {userId} = jwt.verify(token, process.env.APP_SECRET);
    req.userId = userId;
  }
  next();
});

// populate the user on each request
server.express.use(async (req, res, next) => {
  // return if not logged in
  if(!req.userId) return next();

  req.user = await db.query.user({where:{id: req.userId}},
                                 '{ id, permissions, email, name }');
  next();
});


server.start({
  cors: {
    credentials: true,
    origin: process.env.FRONTEND_URL // prevents cross-origin- which prevents playground from working
  }
}, res => {
  console.log(`Server is now running on port http://localhost:${res.port}`)
});