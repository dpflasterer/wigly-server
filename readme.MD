# Wigly
## The Stack
We are using Graphql Yoga with binding to stitch our services to a single endpoint for our front end. 
- Yoga
  - Prisma (MySQL or Cloud)
  - etc...


## Getting Started
### Versions
- node: v10.12.0
- npm: v6.4.1
- nvm: v0.33.8

### Installation
`npm install`

### Configuration
- Create a `.env` file from `.env.sample` and fill in missing credentials.
- Generate schema: `npm run update:schema`
  - Verify `src/generated/prisma.graphql` was created.

### Run
`npm run dev`: Runs the server with verbose logging. Useful for development.
`npm run start`: Runs the server.
`npm run update:schema`: Generates all schemas Prisma
`npm run playground`: Runs an instance of GraphQL Playground in the browser.


## The plan
Leverage Yoga to stich all API and services into a single endpoint for our React + Apollo front end. 
1. Leverage Prisma for User databases.
1. ...


## The Parts
### Prisma
This is where our user database will live.
> Only endpoints exposed via `schama.graphql` AND (`src/resolvers/Query` or `src/resolvers/Mutation`) are available via 
> the Yoga server. New Queries and Mutations must be manually forwarded.

Note: I was working through the Wes Bos Advanced React class so their may be traces of examples that have nothing to do
with furthermore. Items, for example.

---
